import unittest

from recoverymodel import (
    RecoveryModel,
    generic_linear_func,
    generic_log_func,
    generic_logistic_func,
)


def _get_basic_params():
    forecast = [100] * 10
    lockdowns = [
        {
            'start': 1,
            'length': 3,
            'immediate_loss': 0.9,
            'client_deaths': generic_linear_func(
                x_intercept=0.05, slope=0.05),
            'lockdown_growth': generic_linear_func(
                x_intercept=0.0, slope=0.01)
        }
    ]

    recoveries = [
        {
            'client_recovery': generic_linear_func(
                x_intercept=0.1, slope=0.01),
            'market_appetite': generic_linear_func(0.2, 0.02)
        }
    ]
    return forecast, lockdowns, recoveries


class TestRecoveryModel(unittest.TestCase):

    def test_simple(self):

        forecast, lockdowns, recoveries = _get_basic_params()
        rm = RecoveryModel(
            normal_forecast=forecast,
            lockdowns=lockdowns,
            recoveries=recoveries
        )

        adjusted_forecasts = rm.model_recovery()
        expected_forecasts = [
            # start business as usual
            100.0,
            # start lockdown
            10.0,
            11.0,
            12.0,
            # start recovery
            30.0,   # .12 + ( (.85 + .10) * .20 )
            32.0,   # .12 + ( (.85 + .11) * .22 )
            34.0,   # .12 + ( (.85 + .12) * .24 )
            36.0,   # .12 + ( (.85 + .13) * .26 )
            38.0,   # .12 + ( (.85 + .14) * .28 )
            41.0,   # forecast end
        ]
        self.assertEqual(len(adjusted_forecasts), len(forecast))
        self.assertEqual(adjusted_forecasts, expected_forecasts)

        self.assertEqual(rm._lockdown_intervals, [(1, 3)])
        self.assertEqual(rm._recovery_intervals, [(4, 9)])

    def test_w_log_func(self):
        forecast, lockdowns, recoveries = _get_basic_params()

        lockdowns[0]['lockdown_growth'] = generic_log_func(
            base=20,
        )
        lockdowns[0]['length'] = 5

        rm = RecoveryModel(
            normal_forecast=forecast,
            lockdowns=lockdowns,
            recoveries=recoveries
        )
        adjusted_forecasts = rm.model_recovery()
        print(adjusted_forecasts)

        expected_forecasts = [
            100.0, 10.0, 33.0, 47.0, 56.0, 64.0, 80.0, 82.0, 83.0, 85.0
        ]

        self.assertEqual(adjusted_forecasts, expected_forecasts)

    def test_w_logistic_func(self):
        """In this example, we have a 4 week lockdown where growth is
        represented with a logistic function. The logistic function is
        specified to provide a maximum boost to sales at 0.5 with the midpoint
        (where the function goes from exponential growth to log growth)
        set at week 3. The curve steepness is set to 1 which is the usual.
        """
        forecast, lockdowns, recoveries = _get_basic_params()
        lockdowns[0]['lockdown_growth'] = generic_logistic_func(
            midpoint=3,
            curve_steepness=1,
            max_value=0.5,
        )
        lockdowns[0]['length'] = 4

        rm = RecoveryModel(
            normal_forecast=forecast,
            lockdowns=lockdowns,
            recoveries=recoveries
        )
        adjusted_forecasts = rm.model_recovery()
        print(adjusted_forecasts)

        expected_forecasts = [
            100.0, 12.0, 16.0, 23.0, 35.0, 52.0, 54.0, 56.0, 58.0, 60.0
        ]

        self.assertEqual(adjusted_forecasts, expected_forecasts)
